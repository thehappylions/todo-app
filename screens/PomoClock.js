import React, { useState, useEffect } from 'react';
import ProgressCircle from 'react-native-progress-circle';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { StyleSheet, Text, TouchableOpacity, View, StatusBar, Dimensions } from 'react-native';

const screen = Dimensions.get('window');
const formatNumber = number => `0${number}`.slice(-2);
const pomoRound = 25*60

const getRemaining = (time) => {
    const mins = Math.floor(time / 60);
    const secs = time - mins * 60;
    return { mins: formatNumber(mins), secs: formatNumber(secs) };
}

function Clock() {

  const [remainingSecs, setRemainingSecs] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const { mins, secs } = getRemaining(remainingSecs);

  toggle = () => {
    setIsActive(!isActive);
  }

  reset = () => {
    setRemainingSecs(0);
    setIsActive(false);
  }

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setRemainingSecs(remainingSecs => remainingSecs + 1);
      }, 1000);
      if (remainingSecs == pomoRound) {
        setRemainingSecs(0);
        setIsActive(false);
      }
    } else if (!isActive && remainingSecs !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isActive, remainingSecs]);

  return (
    <View style={styles.container}>

      <View style={styles.clock}>
        <ProgressCircle
          percent={(remainingSecs/pomoRound)*100}
          radius={wp('35')}
          borderWidth={wp('2')}
          color="#3399FF"
          shadowColor="#999"
          bgColor="#fff"
        >
          <StatusBar barStyle="light-content" />
          <Text style={styles.timerText}>{`${mins}:${secs}`}</Text>
        </ProgressCircle>
      </View>

      <TouchableOpacity onPress={this.toggle} style={styles.button}>
          <Text style={styles.buttonText}>{isActive ? 'Pause' : 'Start'}</Text>
      </TouchableOpacity>
    
    </View>
  )
}

export default class PomoClock extends React.Component {
  render() {
    return (
      <Clock/>
    )
  }
}

const styles = StyleSheet.create({
  clock: {
    alignItems: "center",
  },
  time: {
    fontSize: wp('18'),
  },
  container: {
    flex: 1,
    backgroundColor: 'rgb(3, 28, 63)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    marginTop: wp('18'),
    borderRadius: wp('3'),
    width: screen.width / 2.5,
    padding: wp('4'),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(227, 35, 58)', 
  },
  buttonText: {
    fontSize: wp('6'),
    color: '#ffffff'
  },
  timerText: {
    color: '#000',
    fontSize: wp('18'),
  },
});
