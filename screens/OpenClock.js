import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import PomoClock from './PomoClock';
import FlatListTask from '../tabs/FlatListTask';

export default createStackNavigator({
    PomoClock:{screen:PomoClock},
    FlatListTask:{screen:FlatListTask},
})