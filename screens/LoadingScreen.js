import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import * as firebase from 'firebase';

export default class LoadingScreen extends React.Component {

	componentDidMount() {
		this.checkIfLoggedIn();
	};

  checkIfLoggedIn = () => {
    firebase.auth().onAuthStateChanged(
      function(user) {
        console.log('AUTH STATE CHANGED CALLED ')
        if (user) {
          this.props.navigation.navigate('MainActivity');
        } else {
          this.props.navigation.navigate('MainActivity');
        }
      }.bind(this)
    );
  };

	render() {
		return (
			<View style={styles.container}>
				<ActivityIndicator size="large"/>
			</View>
		);
	};
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	}
});