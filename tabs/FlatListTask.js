import React from 'react';
import { Text, View, StyleSheet, Image, FlatList, TouchableOpacity, Button, StatusBar, Dimensions } from 'react-native';
import { CheckBox } from 'native-base';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import ListObjectTask from './ListObjectTask';
import PomoClock from '../screens/PomoClock';

class TaskItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    }
  }

  render() {
    return (
      <View style={styles.task}>
        <View style={{flex:1}}>
          <View style={{flex:1, flexDirection:'row'}}>

            <CheckBox style={styles.checkbox} checked={this.state.checked} 
              onPress={() => this.setState({checked: !this.state.checked})}
            />

            <View style={{flex:1}}>
              <Text style={{fontSize: wp('5'), marginLeft: wp('6'), marginRight: wp('3') }}>{this.props.item.taskName}</Text>  
              <View style={styles.boxFolder}>
                <Ionicons name={'md-folder'} size={wp('6')} color={this.props.item.folderColor}/>
                <Text style={styles.textBox}>{this.props.item.folderName}</Text>
              </View>
            </View>

            <View style={styles.pomo}>
              <TouchableOpacity style={styles.pomo} onPress={() => this.props.navigation.navigate('PomoClock')}>
                <MaterialCommunityIcons name={'play-circle-outline'} size={wp('8')} color={this.props.item.priorColor}/>
              </TouchableOpacity>
              <Text>{this.props.item.pomoRound}</Text>
            </View>

          </View>
        </View>
      </View>
    );
  };
}

export default class FlatListTask extends React.Component {
  render () {
    return (
      <View style={{flex: 1, marginTop: wp('26')}}>
        <FlatList 
          data={ListObjectTask}
          renderItem={({item, index}) => {
            return (
              <TaskItem item={item} index={index}>
              </TaskItem>
            );
          }}
        >  
        </FlatList>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  checkbox: {
    width: wp('7'), 
    height: wp('7'),
    borderRadius: wp('10'), 
    paddingTop: wp('1'), 
    paddingLeft: wp('2'),
  },
  task: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  boxFolder: {
    height: wp('10'),
    flex: 1,
    flexDirection: 'row',
    marginLeft: wp('7'),
    marginTop: wp('1'),
  },
  textBox: {
    fontSize: wp('4'),
    paddingLeft: wp('2'),
    paddingTop: wp('1'),
  },
});